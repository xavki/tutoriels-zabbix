%title: ZABBIX
%author: xavki



███████╗ █████╗ ██████╗ ██████╗ ██╗██╗  ██╗
╚══███╔╝██╔══██╗██╔══██╗██╔══██╗██║╚██╗██╔╝
  ███╔╝ ███████║██████╔╝██████╔╝██║ ╚███╔╝ 
 ███╔╝  ██╔══██║██╔══██╗██╔══██╗██║ ██╔██╗ 
███████╗██║  ██║██████╔╝██████╔╝██║██╔╝ ██╗
╚══════╝╚═╝  ╚═╝╚═════╝ ╚═════╝ ╚═╝╚═╝  ╚═╝

-----------------------------------------------------------------------                                                                             

# ZABBIX : introduction - c'est quoi ??


Zabbix :

	* supervision / monitoring : système, réseau, applicatif...

<br>

	* fondateur Alexei Vladishev

	* première version 2001

	* société créée en 2005

<br>

	* à base de C / php

	* opensource / libre


-----------------------------------------------------------------------                                                                             

# ZABBIX : introduction - c'est quoi ??


Liens : 

<br>

	* github : https://github.com/zabbix/zabbix

<br>

	* officiel : https://www.zabbix.com/

-----------------------------------------------------------------------                                                                             

# ZABBIX : introduction - c'est quoi ??


Différents rôles : 

<br>

	* serveur : gestion du stockage

<br>

	* frontend : interface graphique

<br>

	* agents : différents OS (linux, windows, mac...)

<br>

	* proxies : collecte groupée

<br>

	* base de données (hors zabbix) : mysql, postgresql...

-----------------------------------------------------------------------                                                                             

# ZABBIX : introduction - c'est quoi ??


Collecte :

	* agents > centralisation (différence avec prometheus)

	* agents/proxies

	* scripts

	* SNMP (Simple Network Management Protocol)

	* Check basique

-----------------------------------------------------------------------                                                                             

# ZABBIX : introduction - c'est quoi ??


Différents mode de découverte des équipements :

	* agents

	* range d'IP

	* proxy

	* certains services : LDAP...

	* agent SNMP

-----------------------------------------------------------------------                                                                             

# ZABBIX : introduction - c'est quoi ??


Intégration par la communauté :

https://www.zabbix.com/integrations
https://www.zabbix.com/integrations/docker

-----------------------------------------------------------------------                                                                             

# ZABBIX : introduction - c'est quoi ??


Concurrents : 

	* nagios/centreon

	* prometheus/exporters/grafana

	* victoria metrics / vmagent

	* suite elastic partie métriques


-----------------------------------------------------------------------                                                                             

# ZABBIX : introduction - c'est quoi ??


<br>

Programmes :

	* formations

	* certifications

	* supports

<br>

Clin d'oeil (Steve) :

	* https://izi-it.io/

	* https://discord.com/invite/hvauXEQ

