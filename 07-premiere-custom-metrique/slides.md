%title: ZABBIX
%author: xavki


███████╗ █████╗ ██████╗ ██████╗ ██╗██╗  ██╗
╚══███╔╝██╔══██╗██╔══██╗██╔══██╗██║╚██╗██╔╝
  ███╔╝ ███████║██████╔╝██████╔╝██║ ╚███╔╝ 
 ███╔╝  ██╔══██║██╔══██╗██╔══██╗██║ ██╔██╗ 
███████╗██║  ██║██████╔╝██████╔╝██║██╔╝ ██╗
╚══════╝╚═╝  ╚═╝╚═════╝ ╚═════╝ ╚═╝╚═╝  ╚═╝



# ZABBIX : Première Métrique Perso



-----------------------------------------------------------------------                                                                             

# ZABBIX : Première Métrique Perso


```
cat /etc/zabbix/zabbix_agentd.d/user_parameter_sleep.conf 
UserParameter=xavki.sleep,ps aux | grep [s]leep | wc -l 2>/dev/null
```

```
zabbix_agentd -R userparameter_reload
```

Host > Items > Create
