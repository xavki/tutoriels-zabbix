%title: ZABBIX
%author: xavki



███████╗ █████╗ ██████╗ ██████╗ ██╗██╗  ██╗
╚══███╔╝██╔══██╗██╔══██╗██╔══██╗██║╚██╗██╔╝
  ███╔╝ ███████║██████╔╝██████╔╝██║ ╚███╔╝ 
 ███╔╝  ██╔══██║██╔══██╗██╔══██╗██║ ██╔██╗ 
███████╗██║  ██║██████╔╝██████╔╝██║██╔╝ ██╗
╚══════╝╚═╝  ╚═╝╚═════╝ ╚═════╝ ╚═╝╚═╝  ╚═╝

-----------------------------------------------------------------------                                                                             

# ZABBIX : Agent Passif vs Actif

<br>

Qui a en charge l'envoi de la donnée ??


	Passif > serveur zabbix

		* accès firewall aux agents

		* charge importante sur de large infra


-----------------------------------------------------------------------                                                                             

# ZABBIX : Agent Passif vs Actif

<br>

Qui a en charge l'envoi de la donnée ??

	Actif > agent zabbix

		* autorisation FW en sortie pour l'agent

		* charge moindre sur le serveur
