%title: ZABBIX
%author: xavki


███████╗ █████╗ ██████╗ ██████╗ ██╗██╗  ██╗
╚══███╔╝██╔══██╗██╔══██╗██╔══██╗██║╚██╗██╔╝
  ███╔╝ ███████║██████╔╝██████╔╝██║ ╚███╔╝ 
 ███╔╝  ██╔══██║██╔══██╗██╔══██╗██║ ██╔██╗ 
███████╗██║  ██║██████╔╝██████╔╝██║██╔╝ ██╗
╚══════╝╚═╝  ╚═╝╚═════╝ ╚═════╝ ╚═╝╚═╝  ╚═╝



# ZABBIX : introduction - Concepts && Définitions



-----------------------------------------------------------------------                                                                             

# ZABBIX : introduction - Concepts && Défintions

<br>

	* groupes : ensemble d'éléments similaires permettant un conf commune

<br>

	* proxy zabbix : agent intermédiaire permettant de regrouper la collecte d'agent dédié

<br>

	* sonde zabbix : instance ou composant déployé pour surveiller

<br>

	* évènement : seuil ou déclencheur activant une action

<br>

	* trigger : déclenchement ou franchissement d'un seuil d'alerte

<br>

	* webhook : transmission d'informations ou d'action à d'autres app

-----------------------------------------------------------------------                                                                             

# ZABBIX : introduction - Concepts && Défintions


<br>

	* agent actif : l'agent initie et réalise la collecte des données
									(sans intervention du serveur zabbix)

<br>

	* agent passif : 	l'agent collecte les données à la demande
										du serveur zabbix

<br>

	* macro : variable générique définie pour être réutilisée
						ex : {$HOSTNAME} - le host surveillé

<br>

	* low level discovery (LLD) : découverte automatique des éléments à surveiller

-----------------------------------------------------------------------                                                                             

# ZABBIX : introduction - Concepts && Défintions


<br>

	* host template : modèle réutilisable pour configurer le monitoring 
										de hosts spécifiques et similaires

<br>

	* intégrations : plugin permettant la connexions à d'autres outils (docker, clouds...)

<br>

	* notifications : envoyer des messages via des systèmes de communications (sms, email..)

<br>

	* remédiation : lancer des actions pour résoudre des problèmes remontés par la surveillance
									ex : restart de services...

<br>

	* authentification : zabbix permet de coupler son authentification à LDAP et d'autres SSO
